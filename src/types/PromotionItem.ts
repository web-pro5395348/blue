import type { Promotion } from "./Promotion";

type PromotionItem = {
  id: number;
  productid: number | null
  discount: number;
  condition: number
  status: number | null
  // ถ้าอยากให้เหมือนจารย์โกเมศแล้วเวลาเหลือให้กลับมาทำ
};
export { type PromotionItem };