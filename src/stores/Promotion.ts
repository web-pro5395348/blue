import type { Promotion } from '@/types/Promotion';
import { defineStore } from 'pinia'
import { computed, ref } from 'vue';




export const usePromotionStore = defineStore('Promotion', () => {

    const promotionConfirmDialog = ref(false)

    const promotionDialog = ref(false)

    const getStatus = (startDate: string, endDate: string): 'enable' | 'disable' => {
        const currentDate = new Date();

        const year = currentDate.getFullYear();
        const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
        const day = currentDate.getDate().toString().padStart(2, '0');

        const formattedDate = `${year}-${month}-${day}`;

        const start = new Date(startDate.replace(/-/g, '/')); // Fix for Safari and Firefox
        const end = new Date(endDate.replace(/-/g, '/'));

        // Use getTime() for proper comparison
        return start.getTime() <= currentDate.getTime() && currentDate.getTime() <= end.getTime() ? 'enable' : 'disable';
    };

    const promotions = ref<Promotion[]>([{
        id: 1,
        code: 'Member',
        name: '50 baht Discount',
        startdate: '2024-01-9',
        enddate: '2024-01-30',
        get status() {
            return getStatus(this.startdate, this.enddate);
        },
        PromotionItem: [{
            id: 101,
            productid: null,
            discount: 50,
            condition: 0,
            status: null
        }],
    },
    {
        id: 2,
        code: 'AUTUMN15',
        name: 'Autumn Sale',
        startdate: '2022-09-01',
        enddate: '2022-09-30',
        get status() {
            return getStatus(this.startdate, this.enddate);
        },
        PromotionItem: [{
            id: 102,
            productid: null,
            discount: 15,
            condition: 100,
            status: null
        }],
    },
    {
        id: 3,
        code: 'WINTER10',
        name: 'Winter Sale',
        startdate: '2022-12-01',
        enddate: '2022-12-31',
        get status() {
            return getStatus(this.startdate, this.enddate);
        },
    },
    {
        id: 4,
        code: 'BLACKFRIDAY',
        name: 'Black Friday',
        startdate: '2022-11-26',
        enddate: '2022-11-30',
        get status() {
            return getStatus(this.startdate, this.enddate);
        },
    },
    {
        id: 5,
        code: 'CYBERMONDAY',
        name: 'Cyber Monday',
        startdate: '2024-01-01',
        enddate: '2024-11-29',
        get status() {
            return getStatus(this.startdate, this.enddate);
        },
        PromotionItem: [{
            id: 103,
            productid: null,
            discount: 20,
            condition: 100,
            status: null
        }],
    }]);
    // this is about search things

    const searchQuery = ref('');
    // chat gpt mod this to sort kub
    const filteredPromotions = computed(() => {
        const filtered = promotions.value.filter((promotion) =>
            promotion.name.toLowerCase().includes(searchQuery.value.toLowerCase())
        );

        // Custom sorting function
        filtered.sort((a, b) => {
            // 'enable' promotions come first
            if (a.status === 'enable' && b.status !== 'enable') {
                return -1;
            } else if (a.status !== 'enable' && b.status === 'enable') {
                return 1;
            }

            // Sort by other criteria (you can adjust this based on your needs)
            // For example, sorting by start date
            const dateA = new Date(a.startdate).getTime();
            const dateB = new Date(b.startdate).getTime();

            return dateA - dateB;
        });

        return filtered;
    });


    const currentPromotion = ref<Promotion>({
        id: -1,
        code: '',
        name: 'Hello',
        startdate: '2024-01-09',
        enddate: '2024-01-30',
        get status() {
            return getStatus(this.startdate, this.enddate);
        },
        PromotionItem: []
    })

    return {

        promotionDialog, promotions, promotionConfirmDialog, getStatus, filteredPromotions, searchQuery, currentPromotion


    }
})
