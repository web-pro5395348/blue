import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import router from '@/router'
import { useRouter } from 'vue-router'

export const useAuthStore = defineStore('auth', () => {
  const currentUser = ref<User[]>([
    {
      id: 1,
      username: 'blue',
      password: '1234',
      fullName: 'เจษฎา นาที',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 2,
      username: 'jaa',
      password: '1234',
      fullName: 'ภสิณี ฉวีเนตร์',
      gender: 'male',
      roles: ['user']
    }
  ])

  const username = ref('')
  const password = ref('')
  const router = useRouter()
  const loading = ref(false)
  const dialogFailed = ref(false)
  function login() {
    const foundUser = currentUser.value.find(
      (item) => item.username === username.value && item.password === password.value
    )
    if (foundUser) {
      router.push('/pos')
      useAuthStore().setLoggedInUser(foundUser.fullName)
    } else {
      dialogFailed.value = true
      return
    }
  }

  const loggedInUser = ref<{ fullName: string} | null>(null)
  function setLoggedInUser(fullName: string) {
    loggedInUser.value = { fullName }
  }

  function closeDialog() {
    dialogFailed.value = false
    clear()
  }
  function clear() {
    username.value = ''
    password.value = ''
    setLoggedInUser('')
  }
  function logout() {
    clear()
    router.push('/login')
  }

  return {
    currentUser,
    username,
    password,
    loggedInUser,
    dialogFailed,
    loading,
    login,
    setLoggedInUser,
    closeDialog,
    logout
  }
})
